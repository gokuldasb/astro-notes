Terms / പദങ്ങള്‍
=============

## Generic Terms / സാമാന്യ പദങ്ങള്‍
| Term/പദം | Meaning/അര്‍ത്ഥം |
|----------|---------------|
| ഏകകം | Unit |
| ബലം | Force |
| ശക്തി | Power |
| ഊര്‍ജ്ജം | Energy |

## Astronomical Terms / ജ്യോതിഷ പദങ്ങള്‍
| Term/പദം | Meaning/അര്‍ത്ഥം |
|----------|---------------|
| ക്രാന്തീപഥം/ക്രാന്തീവൃത്തം | Ecliptic |
| വൃദ്ധിക്ഷയം ചക്രം | Lunar Cycle (synodic) |
| പൗര്‍ണ്ണമി | Full moon |
| അമാവസി | New moon |
| പക്ഷം | Time between full/new moons |
| ശുക്ലപക്ഷം / വെളുത്തപക്ഷം | Time from new moon to full moon |
| കൃഷ്ണപക്ഷം / കറുത്തപക്ഷം | Time from full moon to new moon |
| രാഹു | Ascending node of lunar orbit |
| കേതു | Descending node of lunar orbit |
| അക്ഷാംശം | Latitude |
| രേഖാംശം | Longtitude |
| വിഷുസ്ഥാനം | First point of Aries / Vernal Equinox |

## Time / സമയം
- നാഴിക: 24 minutes (ഒരു ദിവസം 60 നാഴിക)
- വിനാഴിക: 24 seconds (ഒരു നാഴിക 60 വിനാഴിക)
- ഗുര്‍വക്ഷരം: 0.4 seconds (ഒരു വിനാഴിക 60 ഗുര്‍വക്ഷരം)
- യാമം: 3 hours, 7.5 യാമം
    - ഒരു ദിവസം 8 യാമം
    - Starts at 3 AM (സരസ്വതീയാമം)
- തിഥി: ഒരു പക്ഷത്തിലെ ഒരു ദിവസം
    - പ്രഥമ: 1st day after new/full moons
    - ദ്വിതീയ: 2nd day
    - തൃതീയ: 3rd day
    - ചതുര്‍ത്ഥി: 4th day
    - പഞ്ചമി: 5th day
    - ഷഷ്ഠി: 6th day
    - സപ്തമി: 7th day
    - അഷ്ഠമി: 8th day
    - നവമി: 9th day
    - ദശമി: 10th day
    - ഏകാദശി: 11th day
    - ദ്വാദശി: 12th day
    - ത്രയോദശി: 13th day
    - ചതുര്‍ദശി: 14th day
    - പഞ്ചദശി: അമാവസി / പൗര്‍ണ്ണമി
