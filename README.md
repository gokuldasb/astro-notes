My Astronomy Notes
==================

This tree contains my notes on Astronomy. The contents may be used under the
terms of CC-BY 4.0 license.

- Malayalam
    - [Terms / പദങ്ങള്‍](malayalam/terms.md)
    - [Stars / നക്ഷത്രങ്ങള്‍](malayalam/nakshatram.md)
